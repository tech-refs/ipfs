## First node:

first console:

```sh
wget https://dist.ipfs.io/go-ipfs/v0.4.20/go-ipfs_v0.4.20_linux-amd64.tar.gz && \
tar xvfz go-ipfs_v0.4.20_linux-amd64.tar.gz && \
cd go-ipfs && \
./install.sh

ipfs init
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["http://89.223.93.204:5001", "http://127.0.0.1:5001", "https://webui.ipfs.io"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["PUT", "GET", "POST"]'
ipfs config Addresses.API '/ip4/0.0.0.0/tcp/5001'
ipfs daemon
```

second console:

```sh
echo "Hello world" > data.json
ipfs files mkdir -p /quotes/2019/04/03
HASH=$(ipfs add -q data.json)
ipfs files cp /ipfs/$HASH /quotes/2019/04/03/data.json
ipfs files ls -l
HASH_QUOTES
```

## Main node:

first console:

```sh
wget https://dist.ipfs.io/go-ipfs/v0.4.20/go-ipfs_v0.4.20_linux-amd64.tar.gz && \
tar xvfz go-ipfs_v0.4.20_linux-amd64.tar.gz && \
cd go-ipfs && \
./install.sh

ipfs init
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["http://89.223.90.194:5001", "http://127.0.0.1:5001", "https://webui.ipfs.io"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["PUT", "GET", "POST"]'
ipfs config Addresses.API '/ip4/0.0.0.0/tcp/5001'
ipfs daemon
```

second console:

```sh
ipfs object new
HASH_OBJECT
ipfs object patch add-link $HASH_OBJECT FUSD-FEUR /ipfs/$HASH_QUOTES
UPDATED_OBJECT
ipfs object new
HASH_OBJECT_2
ipfs object patch add-link $HASH_OBJECT_2 quotes /ipfs/$UPDATED_OBJECT
NEW_HASH_DIR
ipfs name publish $NEW_HASH_DIR
Published to QmbZdHFjHfPDoVGHRwNkW2nutuRRhSrY9fKgsRiT5hPrUu: /ipfs/QmcoLC41r3fmQ6xsfkj3D6JToVkNT4QhDjTHFpzwKhRALQ
curl "http://localhost:5001/api/v0/name/resolve?arg=$HASH_MAIN_NODE"
{"Path":"/ipfs/HASH_PUBLIC"}
curl "http://89.223.28.86:5001/api/v0/cat?arg=/ipfs/$HASH_PUBLIC/quotes/FUSD-FEUR/2019/04/03/data.json"
```