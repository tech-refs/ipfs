package ipfs

import (
	"fmt"

	shell "github.com/ipfs/go-ipfs-api"
)

// Client structure represent shell with client to work with IPFS storage
type Client struct {
	shell *shell.Shell
}

// NewClient returns IPFSclient to work with IPFS on host
func NewClient(host string) (*Client, error) {
	c := &Client{shell: shell.NewShell(host)}
	if !c.shell.IsUp() {
		return nil, fmt.Errorf("IPFS host is not available")
	}
	return c, nil
}

//PutDir puts files from dir to IPFS and returns directory hash and error
func (c *Client) PutDir(dir string) (string, error) {
	cid, err := c.shell.AddDir(dir)
	if err != nil {
		return "", fmt.Errorf("Uploading data to storage failed. Error: %s", err)
	}
	return cid, nil
}
