package main

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/tech-refs/ipfs/instructions/go/adddirectory/ipfs"
)

const ipfsHost = "localhost:5001"

func main() {

	ipfsClient, err := ipfs.NewClient(ipfsHost)
	if err != nil {
		panic(err)
	}
	dirWd, _ := os.Getwd()
	dir := filepath.Join(dirWd, "/tmp")
	str, err := ipfsClient.PutDir(dir)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", str)

}
