# Reference of sending and getting file from IPFS

## Prepare

[Prepare manual for running ipfs](../../docker/base-setup/README.md)

## Create directory and files

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/go/adddirectory && \
mkdir -p tmp && \
echo -e "hello world" > tmp/hello.txt && \
echo -e "hello world again" > tmp/hello_again.txt
```

## Run

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/go/adddirectory && \
go get ./... && \
go run main.go
```
