package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	shell "github.com/ipfs/go-ipfs-api"
)

const host = "localhost:5001"

type jsonFile struct {
	CurrentTime time.Time `json:"time"`
}

func main() {

	path, err := makeDir("tmp")
	if err != nil {
		panic(err)
	}

	jsonFile, err := makeJSON(path, "currenttime.json")
	if err != nil {
		panic(err)
	}

	fileReader, err := os.Open(jsonFile)
	if err != nil {
		panic(fmt.Errorf("Opening file %s failed. Error: %s", jsonFile, err))
	}

	sh := shell.NewShell(host)
	cid, err := sh.Add(fileReader)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %s", err)
		os.Exit(1)
	}
	fmt.Printf("added %s\n", cid)

	err = sh.Get(cid, path)
	if err != nil {
		panic(err)
	}
}

func makeDir(path string) (string, error) {
	wd, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf("Getting current directory failed. Error; %s", err)
	}
	newPath := filepath.Join(wd, path)
	err = os.MkdirAll(newPath, os.ModePerm)
	if err != nil {
		return "", fmt.Errorf("Creating directory %s failed. Error; %s", newPath, err)
	}
	return newPath, nil
}

func makeJSON(path, name string) (filePath string, err error) {
	filePath = filepath.Join(path, name)
	JSONtimeMarshalled, err := json.Marshal(&jsonFile{CurrentTime: time.Now()})
	if err != nil {
		return filePath, fmt.Errorf("Marshaling to json failed. Error: %s", err)
	}
	err = ioutil.WriteFile(filePath, JSONtimeMarshalled, os.ModePerm)
	if err != nil {
		return filePath, fmt.Errorf("Writing json is failed. Error: %s", err)
	}
	return filePath, nil
}
