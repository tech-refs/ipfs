# Reference of sending and getting file from IPFS

## Prepare

[Prepare manual for running ipfs](../../docker/base-setup/README.md)


## Run

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/go/writefile/ && \
go get ./... && \
go run main.go
```
