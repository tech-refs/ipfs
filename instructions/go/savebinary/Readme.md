# Reference of sending and getting file from IPFS

## Prepare

[Prepare manual for running ipfs](../../docker/base-setup/README.md)

## Run reference program

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/go/savebinary && \
go get ./... && \
go run main.go
```

## Run IPFS client tests

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/go/savebinary/test && \
go test -v
```

## Run IPFS client tests with coverage

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/go/savebinary/test && \
ginkgo -coverpkg gitlab.com/tech-refs/ipfs/instructions/go/ipfs/savebinary/ipfs
```