package ipfs

import (
	"archive/tar"

	shell "github.com/ipfs/go-ipfs-api"
)

type IPFSReadCloser struct {
	tr   *tar.Reader
	resp *shell.Response
}

//Read reads data from the tar.Reader
func (rc *IPFSReadCloser) Read(b []byte) (int, error) {
	return rc.tr.Read(b)
}

//Close closes the response reader
func (rc *IPFSReadCloser) Close() error {
	return rc.resp.Close()
}
