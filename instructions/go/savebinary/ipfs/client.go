package ipfs

import (
	"archive/tar"
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"

	shell "github.com/ipfs/go-ipfs-api"
)

// Client structure represent shell with client to work with IPFS storage
type Client struct {
	shell *shell.Shell
}

// NewClient returns IPFSclient to work with IPFS on host
func NewClient(host string) (*Client, error) {
	c := &Client{shell: shell.NewShell(host)}
	if !c.shell.IsUp() {
		return nil, fmt.Errorf("IPFS host is not available")
	}
	return c, nil
}

//PutByReader puts data to IPFS from io.Reader and returns file hash and error
func (c *Client) PutByReader(r io.Reader) (string, error) {
	cid, err := c.shell.Add(r)
	if err != nil {
		return "", fmt.Errorf("Uploading data to storage failed. Error: %s", err)
	}
	return cid, nil
}

//Put puts data to IPFS from []byte and returns file hash and error
func (c *Client) Put(b []byte) (string, error) {
	return c.PutByReader(bytes.NewReader(b))
}

//GetByReadCloser returns Reader or error
func (c *Client) GetByReadCloser(hash string) (io.ReadCloser, error) {
	resp, err := c.shell.Request("get", hash).Send(context.Background())
	if err != nil {
		return nil, err
	}
	if resp.Error != nil {
		return nil, fmt.Errorf("Getting file by hash %s failed. Error: %s", hash, resp.Error)
	}
	tr := tar.NewReader(resp.Output)
	_, err = tr.Next()
	switch {
	case err == io.EOF:
		return nil, fmt.Errorf("Get empty archive by hash %s", hash)
	case err != nil:
		return nil, err
	}
	return &IPFSReadCloser{
		resp: resp,
		tr:   tr,
	}, nil
}

//Get returns []byte of file or error
func (c *Client) Get(hash string) ([]byte, error) {
	rc, err := c.GetByReadCloser(hash)
	if err != nil {
		return nil, err
	}
	defer rc.Close()
	return ioutil.ReadAll(rc)
}
