package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"time"

	"gitlab.com/tech-refs/ipfs/instructions/go/savebinary/ipfs"
)

type jsonFile struct {
	CurrentTime time.Time `json:"time"`
}

const host = "localhost:5001"

func main() {
	ipfsClient, err := ipfs.NewClient(host)
	if err != nil {
		panic(err)
	}

	JSONtimeMarshalled, err := json.Marshal(&jsonFile{CurrentTime: time.Now()})
	if err != nil {
		panic(fmt.Errorf("Marshaling to json failed. Error: %s", err))
	}

	br := bytes.NewReader(JSONtimeMarshalled)
	cidFromBytesReader, err := ipfsClient.PutByReader(br)
	if err != nil {
		panic(err)
	}

	rc, err := ipfsClient.GetByReadCloser(cidFromBytesReader)
	if err != nil {
		panic(err)
	}
	defer rc.Close()

	bytesFromTarReader, err := ioutil.ReadAll(rc)
	if err != nil {
		panic(err)
	}
	fmt.Println("Data from tar reader")
	fmt.Printf("%+v\n", bytesFromTarReader)
	fmt.Printf("%+v\n", string(bytesFromTarReader))

	cidFromBytes, err := ipfsClient.Put(JSONtimeMarshalled)
	if err != nil {
		panic(err)
	}
	bytes, err := ipfsClient.Get(cidFromBytes)
	if err != nil {
		panic(err)
	}
	fmt.Println("Data from bytes")
	fmt.Printf("%+v\n", bytes)
	fmt.Printf("%+v\n", string(bytes))
}
