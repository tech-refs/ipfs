package ipfsclient_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/tech-refs/ipfs/instructions/go/savebinary/ipfs"
)

var _ = Describe("IpfsClient", func() {

	const host = "localhost:5001"

	var (
		ipfsClient     *Client
		hash           string
		jsonMarshalled []byte
		err            error
	)

	BeforeEach(func() {
		jsonMarshalled, _ = json.Marshal(time.Now())
	})

	Describe("Creating IPFS client", func() {
		It("should pass", func() {
			ipfsClient, err = NewClient(host)
			Expect(err).NotTo(HaveOccurred())
		})
	})

	Describe("Putting data to IPFS", func() {
		Context("from reader", func() {
			It("should pass", func() {
				r := bytes.NewReader(jsonMarshalled)
				hash, err = ipfsClient.PutByReader(r)
				Expect(err).NotTo(HaveOccurred())
				bytes, err := ipfsClient.Get(hash)
				Expect(err).NotTo(HaveOccurred())
				Expect(bytes).Should(Equal(jsonMarshalled))
			})
		})
		Context("from bytes", func() {
			It("should pass", func() {
				hash, err = ipfsClient.Put(jsonMarshalled)
				Expect(err).NotTo(HaveOccurred())
				bytes, err := ipfsClient.Get(hash)
				Expect(err).NotTo(HaveOccurred())
				Expect(bytes).Should(Equal(jsonMarshalled))
			})
		})
	})

	Describe("Reading data from IPFS", func() {
		BeforeEach(func() {
			hash, _ = ipfsClient.Put(jsonMarshalled)
		})
		Context("to reader", func() {
			It("should pass", func() {
				tr, err := ipfsClient.GetByReadCloser(hash)
				defer tr.Close()
				Expect(err).NotTo(HaveOccurred())
				bytes, err := ioutil.ReadAll(tr)
				Expect(err).NotTo(HaveOccurred())
				Expect(bytes).Should(Equal(jsonMarshalled))
			})
		})
		Context("to bytes", func() {
			It("should pass", func() {
				bytes, err := ipfsClient.Get(hash)
				Expect(err).NotTo(HaveOccurred())
				Expect(bytes).Should(Equal(jsonMarshalled))
			})
		})
	})
})
