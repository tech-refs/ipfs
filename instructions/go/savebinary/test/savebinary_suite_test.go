package ipfsclient_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestSavebinary(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Savebinary Suite")
}
