
# IPFS references

## Docker

It is assumed that you have golang install and clone this repo to $GOPATH as appropriate.

[IPFS launch](docker/base-setup/README.md)

Steps to run docker with ipfs

[IPFS with data volume launch](docker/with-data-volume/README.md)

Steps to run ipfs docker with data volume (saving data on host)

[IPFS with export volume and loading directory](docker/add-directory/README.md)

Steps to run ipfs docker with export volume (download directory to ipfs)

[Go Test IPFS adding and getting file](docker/go-test/README.md)

Reference of docker compose with ipfs and golang test in ginkgo

[Pin file from third-parrty node](docker/pinning/README.md)

Steps to run ipfs docker with export volume (download directory to ipfs)

## Go

[Send and get file from IPFS](go/writefile/Readme.md)

Reference of using go-ipfs-api for sending and getting file from IPFS

[Add directory to IPFS](go/adddirectory/Readme.md)

Reference of using go-ipfs-api for adding directory with files to IPFS

[Send and get file from IFPS](go/savebinary/Readme.md)

Reference of using IPFSclient package for sending and getting file from IPFS

## Bare metall

[Create 2 IPFS nodes](bare-metall/Readme.md)

Reference of staring 2 global IPFS nodes

## Remote ipfs

[Coonect to remote IPFS host](remote-ipfs.md)