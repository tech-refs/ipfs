# How to run IPFS docker image

## Run ipfs node

### Run ipfs local host in docker

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/base-setup && \
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true && \
docker run -d \
    --name ipfs \
    -e IPFS_PROFILE=test \
    -p 4001:4001 \
    -p 127.0.0.1:8080:8080 \
    -p 127.0.0.1:5001:5001 \
    ipfs/go-ipfs:latest
```

### Or run ipfs global host in docker

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/base-setup && \
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true && \
docker run -d \
    --name ipfs \
    -p 4001:4001 \
    -p 127.0.0.1:8080:8080 \
    -p 127.0.0.1:5001:5001 \
    ipfs/go-ipfs:latest
```

## Working with ipfs

### Create file

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/base-setup && \
mkdir -p tmp && TMP=$(pwd)/tmp && \
echo -e "hello world" > $TMP/hello.txt
```

### Add file to ipfs

```sh
FILE=$(curl -s -F "file=@$TMP/hello.txt" "http://127.0.0.1:5001/api/v0/add") && echo $FILE && \
HASH=$(echo $FILE | jq '.Hash') && HASH="${HASH//\"}" && echo $HASH
```

### Cat file

```sh
curl -s "http://127.0.0.1:5001/api/v0/cat?arg=$HASH"
```

### Get file

```sh
curl -s "http://127.0.0.1:5001/api/v0/get?arg=$HASH" -o $TMP/$HASH.tar && tar -xvf $TMP/$HASH.tar -C $TMP && \
cat $TMP/$HASH
```

## Stop docker if desired

```sh
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true
```