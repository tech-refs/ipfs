#!/bin/sh

set -e

ipfs init
ipfs config --json Addresses.Announce "[\"/ip4/$ANNOUNCE_IP/tcp/$ANNOUNCE_PORT\"]"
# cat /data/ipfs/config | jq '.Identity.PrivKey = $privKey' --arg privKey $PRIV_KEY | jq '.Identity.PeerID = $peerID' --arg peerID $PEER_ID > tmp.json && mv tmp.json /data/ipfs/config
ipfs config Addresses.API '/ip4/0.0.0.0/tcp/5001'
# ipfs config Addresses.Gateway '/ip4/0.0.0.0/tcp/8080'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["http://'$ANNOUNCE_IP':'$ANNOUNCE_PORT'", "http://127.0.0.1:'$ANNOUNCE_PORT'", "https://webui.ipfs.io"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["PUT", "GET", "POST"]'
cat /data/ipfs/config
ipfs daemon