# How to run IPFS docker image

## Run ipfs node

### Run ipfs local host in docker

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/with-config && \
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true && \
docker run -d \
    --name ipfs \
    -e IPFS_PROFILE=test \
    -p 4001:4001 \
    -p 127.0.0.1:8080:8080 \
    -p 127.0.0.1:5001:5001 \
    ipfs/go-ipfs:latest
```

### Or run ipfs global host in docker

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/with-config && \
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true && \
docker run -d \
    --name ipfs \
    -v $PWD/config/config:/data/ipfs/config \
    -v $PWD/config/version:/data/ipfs/version \
    -p 4001:4001 \
    -p 127.0.0.1:8080:8080 \
    -p 127.0.0.1:5001:5001 \
    ipfs/go-ipfs:latest
```




cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/with-config && \
docker build -t ipfs . && \
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true && \
docker run --rm -it \
    --name ipfs \
    -p 4001:4001 \
    -p 8080:8080 \
    -p 5001:5001 \
    -e ANNOUNCE_IP=89.223.95.138 \
    -e ANNOUNCE_PORT=5001 \
    -e PEER_ID="QmTerQNN3V3S22DHFAsV3tnkEk5DLFFMGaQgW2HaQJpwQv" \
    -e PRIV_KEY="CAASpwkwggSjAgEAAoIBAQDbNu2KEDA36o1rw1C6xEeeSfTCXWmHQsdht7ArLWo5vQ2k+MWJ2MExZPQhzKEzSMOGRxPwu0w9pzYcBkUYzIqzAgM32rIjyKQA8ftsxUb6gIDYYjulqqa4v9hz0TQvtYUiJ6oEDNU1lHrSeDaLF/42WVG8bd6yKlJlon+A53LKfs+1IX9Q3JhxEm5HT0WhY46t1ZHyw7/m88OGLBKJhQuB/xooxPgh9rGkukbEknBQpvHDxREf6ZgYvOeh6ph8IPowd+cfbRrQySiyDZjnmA3cbuigWp/GEsZ9eb8hZN5uZ2NExRN1fskAtog9brONZKR0yzHNko+rENrHd8VOjlH1AgMBAAECggEAOMUExxhiJFjtvtaZ9YQXN/rXE+61AX5dvASh7syYEdPP0MVMAuLLRUtrncUsiedyGzgVBea7W8wNyzwkcMOnt6vxwWk1cObo8gW4ovaD5yOYOnFWGRUUkAu+tl+bMC/GBo5XDS11maaWGCNhnKvd/Z0r+axKW8FyDTgD5RILPD0YOw552OigPXRVX9DLstmXmDZRA/qXSwIntpW+ik9YWRWDDg0sLfj5vOtQzCNXs+es1BreMMOOkkQgGix0MxlNbGE3mF3P20+QwdNnUW0SCGTO4oKnhhWf4ZumcZZNu0ykXzPwOMQP/SEstFxAEV3eeQRxkZcrWWiJ+xLBiW6YgQKBgQDnxgEasaIPqH5hjRefRdTCng60iVk+u34qlw2ybOydOanlyEYWz+4ynkYvGVmI1Bh5G+e1ZEPE5Xs8FZc3btV1xhYFI0lUFB0cWd4204cW2nLr0MdDndBfgtWjeS2NdQX4BNEi7REOeUmK+Qnv478aE6Hxb8uoZV9l8RqmyHvcTQKBgQDyINyfo3mghiQlhdBsrtxKUJuWHzXX6CoT4owMAR1eSZJhwVRPgSMbr45Adpu8HgWAI/pIyyCdt1lJG41DvGyDLvkkY7aRNAXTPzRAwtmV969Aspd+DeA1Rl49XZIhWI2HTspgrzgzTRCHaRAMR/71fYjnOKj2lqn77rpTvG6ASQKBgDhLYY1IQ+KlvDyZdnlbDrhOh6fWDHUuORDDK7KAvl/Z16hWIp/71RE6XDWoZJTVXzRbUMZAGi3Cx1HqtJXkN7ipakPJNFBVqlRuh4itjAxC9tLMHPoRZMEuGBHipuvK1q5NDTb8eb4YO4KjxoQbvxjoV9c6ni7CoI80JpMH8v2pAoGAe5m45ik+Wmp/o0P3DLQKskkJ042lH81dRX9dfHdJNaua9RoK3vcOJADvPgsOHu64Yhx7lJNU2A4TnxVYfJ6ASks9VY0tUlxbKOZX10NgbYTdysogOXH95VRBoVPV1/3raYoYF1j6+H07ldRVI9/Q0CLAM0CiSBbLOVc5eKt1QTkCgYEAsIxGh1cBv5X2OpEQjiRrxnSo6/5lEjxxOakqystZiTj09q3CLKOh0Z4CyazCyPzqert/NG9VFFhXF0XIPM2r8dh2hYLeUPTIqn1swulkFhm7prTm1PLDGqvj6WxgjtJGsGFNVeaRDVhGpkejsAwc2Rsz0rb2lfWmSxEpREjEW7g=" \
    ipfs








## Working with ipfs

### Create file

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/with-config && \
mkdir -p tmp && TMP=$(pwd)/tmp && \
echo -e "hello world" > $TMP/hello.txt
```

### Add file to ipfs

```sh
FILE=$(curl -s -F "file=@$TMP/hello.txt" "http://127.0.0.1:5001/api/v0/add") && echo $FILE && \
HASH=$(echo $FILE | jq '.Hash') && HASH="${HASH//\"}" && echo $HASH
```

### Cat file

```sh
curl -s "http://127.0.0.1:5001/api/v0/cat?arg=$HASH"
```

### Get file

```sh
curl -s "http://127.0.0.1:5001/api/v0/get?arg=$HASH" -o $TMP/$HASH.tar && tar -xvf $TMP/$HASH.tar -C $TMP && \
cat $TMP/$HASH
```

## Stop docker if desired

```sh
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true
```