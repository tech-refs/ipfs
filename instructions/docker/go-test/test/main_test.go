package main_test

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	shell "github.com/ipfs/go-ipfs-api"
)

const host = "ipfs:5001"

var _ = Describe("Main", func() {

	var sh *shell.Shell
	var path, jsonFile string
	var err error
	var reader io.Reader

	BeforeEach(func() {
		sh = shell.NewShell(host)
		path, err = makeDir("tmp")
		if err != nil {
			panic(err)
		}
		jsonFile, err = makeJSON(path, "test.json")
		if err != nil {
			panic(err)
		}
		reader, err = os.Open(jsonFile)
		if err != nil {
			panic(err)
		}
	})

	Describe("Adding and getting file from IPFS", func() {
		It("should pass", func() {
			cid, err := sh.Add(reader)
			Expect(err).To(BeNil())
			Expect(sh.Get(cid, path)).Should(Succeed())
		})
	})
})

type jsonFile struct {
	CurrentTime time.Time `json:"time"`
}

func makeDir(path string) (string, error) {
	wd, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf("Getting current directory failed. Error; %s", err)
	}
	newPath := filepath.Join(wd, path)
	err = os.MkdirAll(newPath, os.ModePerm)
	if err != nil {
		return "", fmt.Errorf("Creating directory %s failed. Error; %s", newPath, err)
	}
	return newPath, nil
}

func makeJSON(path, name string) (filePath string, err error) {
	filePath = filepath.Join(path, name)
	JSONtimeMarshalled, err := json.Marshal(&jsonFile{CurrentTime: time.Now()})
	if err != nil {
		return filePath, fmt.Errorf("Marshaling to json failed. Error: %s", err)
	}
	err = ioutil.WriteFile(filePath, JSONtimeMarshalled, os.ModePerm)
	if err != nil {
		return filePath, fmt.Errorf("Writing json is failed. Error: %s", err)
	}
	return filePath, nil
}
