package main_test

import (
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestCurrentTime(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "CurrentTime Suite")
}
