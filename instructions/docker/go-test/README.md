# Run test in docker

## Up Docker compose

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/go-test && \
docker-compose up --build --exit-code-from go_test && \
docker-compose down
```

## Check exit code

```sh
echo $?
```