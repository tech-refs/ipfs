# How to run IPFS docker image with data volume

## Workflow to check data

1. Run ipfs local host in docker
2. Add file to ipfs
3. Cat file
4. Restart ipfs docker
5. Cat file

## Run ipfs local host in docker

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/with-data-volume && \
mkdir ipfs_data || true && \
export IPFS_DATA=$PWD/ipfs_data && \
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true && \
docker run -d \
    --name ipfs \
    -e IPFS_PROFILE=test \
    -p 4001:4001 \
    -p 127.0.0.1:8080:8080 \
    -p 127.0.0.1:5001:5001 \
    -v $IPFS_DATA:/data/ipfs \
    ipfs/go-ipfs:latest
```

## Working with ipfs

### Create file

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/with-data-volume && \
mkdir -p tmp && TMP=$PWD/tmp && \
echo -e "hello world" > $TMP/hello.txt
```

### Add file to ipfs

```sh
FILE=$(curl -s -F "file=@$TMP/hello.txt" "http://127.0.0.1:5001/api/v0/add") && echo $FILE && \
HASH=$(echo $FILE | jq '.Hash') && HASH="${HASH//\"}" && echo $HASH
```

### Cat file

```sh
curl -s "http://127.0.0.1:5001/api/v0/cat?arg=$HASH"
```

## Restart ipfs docker

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/with-data-volume && \
docker stop ipfs && \
docker rm ipfs && \
docker run -d \
    --name ipfs \
    -e IPFS_PROFILE=test \
    -p 4001:4001 \
    -p 127.0.0.1:8080:8080 \
    -p 127.0.0.1:5001:5001 \
    -v $IPFS_DATA:/data/ipfs \
    ipfs/go-ipfs:latest
```

## Working with ipfs

### Cat file

```sh
curl -s "http://127.0.0.1:5001/api/v0/cat?arg=$HASH"
```

### Get file

```sh
curl -s "http://127.0.0.1:5001/api/v0/get?arg=$HASH" -o $TMP/$HASH.tar && tar -xvf $TMP/$HASH.tar -C $TMP && \
cat $TMP/$HASH
```

## Stop docker

```sh
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true
```

## Stop docker with removing data

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/with-data-volume && \
echo docker stop ipfs && \
echo docker rm ipfs && \
sudo rm -rf tmp || true && \
sudo rm -rf ipfs_data || true
```