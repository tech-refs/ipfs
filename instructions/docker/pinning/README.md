# How to pin file from third-parrty node

## Run ipfs global host in docker

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/pinning && \
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true && \
docker run -d \
    --name ipfs \
    -p 4001:4001 \
    -p 127.0.0.1:8080:8080 \
    -p 127.0.0.1:5001:5001 \
    ipfs/go-ipfs:latest
```

## Working with ipfs

### Make tmp directory

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/pinning && \
mkdir -p tmp && TMP=$PWD/tmp
```

### Pin file from third-party node

```sh
CF_HASH=QmXnnyufdzAWL5CqZ2RnSNgPbvCc1ALT73s6epPrRnZ1Xy && \
curl "http://localhost:5001/api/v0/pin/add?arg=$CF_HASH"
```

### List objects pinned to local storage and find file from third-party node

```sh
curl -s "http://localhost:5001/api/v0/pin/ls" | jq '.Keys' && \
curl -s "http://localhost:5001/api/v0/pin/ls" | jq '.Keys' | grep $CF_HASH
```

### Cat file

```sh
curl -s "http://127.0.0.1:5001/api/v0/cat?arg=$CF_HASH"
```

### Get file

```sh
curl -s "http://127.0.0.1:5001/api/v0/get?arg=$CF_HASH" --output $TMP/$CF_HASH.tar && tar -xvf $TMP/$CF_HASH.tar -C $TMP && \
cat $TMP/$CF_HASH
```

## Stop docker

```sh
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf tmp || true
```