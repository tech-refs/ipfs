# How to run IPFS docker image with export volumes (upload and download directories)

## Workflow to check data

1. Run ipfs local host in docker
2. Create directory and files
3. Add directory to ipfs
4. Get directory from IPFS
5. Stop docker

## Run ipfs local host in docker

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/add-directory && \
export IPFS_EXPORT=$PWD/ipfs_export && \
sudo rm -rf $IPFS_EXPORT || true && \
sudo rm -rf $DOWNLOAD || true && \
mkdir ipfs_export || true && \
docker stop ipfs || true && \
docker rm ipfs || true && \
docker run -d \
    --name ipfs \
    -e IPFS_PROFILE=test \
    -p 4001:4001 \
    -p 127.0.0.1:8080:8080 \
    -p 127.0.0.1:5001:5001 \
    -v $IPFS_EXPORT:/export \
    ipfs/go-ipfs:latest
```

## Working with ipfs

### Create directory and files

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/add-directory && \
mkdir -p $IPFS_EXPORT/tmp && \
echo -e "hello world" > $IPFS_EXPORT/tmp/hello.txt && \
echo -e "hello world again" > $IPFS_EXPORT/tmp/hello_again.txt
```

### Add directory to IPFS

```sh
DIR=$(docker exec ipfs ipfs add -r -Q /export/tmp) && \
echo $DIR
```

### Get directory from IPFS

```sh
cd $GOPATH/src/gitlab.com/tech-refs/ipfs/instructions/docker/add-directory && \
mkdir -p download && DOWNLOAD=$PWD/download && \
curl -s "http://127.0.0.1:5001/api/v0/get?arg=$DIR" -o $DOWNLOAD/$DIR.tar && tar -xvf $DOWNLOAD/$DIR.tar -C $DOWNLOAD
```

### Get file from IPFS directory

```sh
curl -s "http://127.0.0.1:5001/api/v0/cat?arg=$DIR/hello.txt"
```

## Stop docker

```sh
docker stop ipfs && \
docker rm ipfs && \
sudo rm -rf $IPFS_EXPORT || true && \
sudo rm -rf $DOWNLOAD || true
```